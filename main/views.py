from django.shortcuts import render, redirect
from django.http import JsonResponse
import requests
import json
from django.contrib.auth.models import User, auth
from django.contrib.auth import logout, authenticate, login

# Create your views here.
def index(request):
    return render(request, 'index.html')

def data(request):
    q = request.GET['q']
    url_tujuan = 'https://www.googleapis.com/books/v1/volumes?q=' + q
    r = requests.get(url_tujuan)
    
    data = json.loads(r.content)

    if not request.user.is_authenticated:
        index = 0
        for i in data['items']:
            del data['items'][index]['volumeInfo']['imageLinks']['smallThumbnail']
            index = index + 1
    
    return JsonResponse(data, safe=False)

def signup(request):
    if request.method == 'POST':
        username = request.POST['username']
        email = request.POST['email']
        password = request.POST['password']

        if User.objects.filter(username=username).exists():
            return redirect('/signup')
        
        elif User.objects.filter(email=email).exists():
            return redirect('/signup')

        else:
            user = User.objects.create_user(username=username, email=email, password=password)
            user.save()
            return redirect('/login')
         
    else:
        return render(request, 'signup.html')

def login(request):
    if request.method == "POST":
        login_username = request.POST['username']
        login_password = request.POST['password']

        user = auth.authenticate(username = login_username, password = login_password)

        if user is not None:
            auth.login(request, user)
            return redirect('/')
        else:
            return redirect('/login/')

    else:
        return render(request, 'login.html')

def logout(request):
    auth.logout(request)
    return redirect('/')