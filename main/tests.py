from django.test import TestCase, Client
from .views import index, data, signup, login, logout
from django.contrib.auth.models import User

# Create your tests here.
class TestStory9(TestCase):
    def test_GET_home(self):
        response = Client().get('/')
        self.assertEquals(response.status_code, 200)

    def test_GET_signup(self):
        response = Client().get('/signup')
        self.assertEquals(response.status_code, 301)

    def test_GET_login(self):
        response = Client().get('/login')
        self.assertEquals(response.status_code, 301)

    def test_GET_logout(self):
        response = Client().get('/logout')
        self.assertEquals(response.status_code, 301)

    def test_signup(self):
        response = self.client.post('/signup',
        {'username':'cinderella', 'email':'cinderella@gmail.com', 'password':'cinderellacantik'}, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_login(self):
        response = self.client.post('/login',
        {'username':'cinderella', 'password':'cinderellacantik'}, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_apakah_story9_ada_templatenya(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')
    
    def test_apakah_ada_text_story9_di_halamannya(self):
        response = Client().get('/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Story 9", html_kembalian)
    
    def test_url_json(self):
        response = Client().get("/data/?q=elsa")
        self.assertEqual(response.status_code, 200)

class LoginTest(TestCase):
    def setUp(self):
        self.credentials = {
            'username' : 'cinderella',
            'password' : 'cinderellacantik'
        }
        User.objects.create_user(**self.credentials)

    def test_login(self):
        response = self.client.post('/login/', self.credentials, follow=True)
        self.assertTrue(response.context['user'].is_authenticated)
